import os, getpass, shutil, subprocess as sub
# Check some dependencies
pass
# Set variables
usr = getpass.getuser()
installdir = "/home/" + usr + "/memux"
cdir = input("Use a custom installation path? Default is ~/memux. (y/N)$ ").lower()
usecustomdir = False
if cdir == 'y':
    usecustomdir = True

def chooseCustomDirectory():
    done = False
    while done == False:
        installdir = str(input("Input a path to install to. $ "))
        if installdir[0:len(installdir)-1] == '/': 
            installdir = installdir[0:len(installdir)-1]
        if installdir[0] == '~':
            installdir = installdir[1:]
            installdir = '/home/'+usr+installdir
        elif installdir[0] == '/':
            pass
        elif installdir[0] == '.':
            installdir = installdir[1:]
            cwd = os.getcwd()
            installdir = cwd + installdir
        else:
            print("Invalid path. Try again.")
            installdir = chooseCustomDirectory()
            break
        if os.path.exists(installdir):
            while 1 == 1:  
                overwrite = str(input("This directory already exists. Do you want to overwrite it? (y/N)$ ")).lower()
                if overwrite == 'y':
                    confirm = str(input("This is not recomended. Proceed? (y/N)$ ")).lower()
                    if confirm != 'y': 
                        break
                    shutil.rmtree(installdir)
                    done = True
                    break    
                else:
                    break
        else:   
            done = True
            
    return installdir

if usecustomdir:
    installdir = chooseCustomDirectory()
elif os.path.exists(installdir):
    g = str(input(installdir + " already exists. Overwrite it? (Y/n)$ ")).lower()
    if g == 'n':
        chooseCustomDir()
    else:
        shutil.rmtree("/home/" + usr + "/memux")


if os.path.exists("/usr/bin/pacman"):
    distro = "arch"
elif os.path.exists("/usr/bin/apt"):
    distro = "debian"
elif os.path.exists("/usr/bin/dnf"):
    distro = "fedora"
elif os.path.exists("/usr/bin/yum"):
    distro = "centos"
elif os.path.exists("/usr/bin/eopkg"):
    distro = "solus"
else:
    print("No package manager detected. Please ensure that all dependencies are installed — refer to README.md for details — then rerun this script.")
    quit()

##########################
### Begin installation ###
##########################

if distro == "arch":
    if not os.path.exists("/usr/bin/tilix"):
        sub.call("yes|sudo pacman -Sy tilix", shell=True)
    if not os.path.exists("/usr/bin/tmux"):
        sub.call("yes|sudo pacman -S tmux", shell=True)
    if not os.path.exists("/usr/bin/pip3"):
        sub.call("yes|sudo pacman -S python-pip libevent", shell=True)

if distro == "debian":
    if not os.path.exists("/usr/bin/tmux"):
        sub.call("yes|sudo apt install tmux", shell=True)
    if not os.path.exists("/usr/bin/tilix"):
        sub.call("yes|sudo apt install tilix", shell=True)
    if not os.path.exists("/usr/bin/pip3"):
        sub.call("yes|sudo apt install python3-pip", shell=True)

if distro == "fedora":
    if not os.path.exists("/usr/bin/tmux"):
        sub.call("yes|sudo dnf install tmux", shell=True)
    if not os.path.exists("/usr/bin/tilix"):
        sub.call("yes|sudo dnf install tilix", shell=True)
    if not os.path.exists("/usr/bin/"): ### Package for pip is currently unknown. Update.
        sub.call("yes|sudo", shell=True)

if distro == "centos":
    if not os.path.exists("/usr/bin/tilix"):
        sub.call("yes|sudo yum install tilix", shell=True)
    if not os.path.exists("/usr/bin/tmux"):
        sub.call("yes|sudo yum install tmux", shell=True)
    if not os.path.exists("/usr/bin/"): ### Package for pip is currently unknown. Update.
        sub.call("yes|sudo", shell=True)

if distro == "solus":
    if not os.path.exists("/usr/bin/tilix"):
        sub.call("sudo eopkg install tilix", shell=True)
    if not os.path.exists("/usr/bin/tmux"):
        sub.call("sudo eopkg install tmux", shell=True)
    sub.call('sudo eopkg install python-psutil', shell=True)
print("Installing to " + installdir)
def clone():
    means = input("Download from Github (1) Sourceforge - recommended (2) $ ")
    if (means != "1" and means != "2"):
        print("Invalid selection. Try again.")
        clone()
    else:
        if means == "1":
            sub.call("mkdir /tmp/memux", shell=True)
            sub.call("mkdir -p " + installdir + ";rmdir " + installdir, shell=True)
            sub.call("git clone https://github.com/alexlucas309/memux.git " + installdir, shell=True)
            if not os.path.exists(installdir):
                print("\nCould not install files to target directory. Check perissions and internet connection.")
                print("\033[1;31;40m FAILED :(")
                quit()

        else:
            sub.call("mkdir /tmp/memux", shell=True)
            sub.call("wget -P /tmp/memux" + " https://master.dl.sourceforge.net/project/memux/memux.tar.xz", shell=True)
            if not os.path.exists("/tmp/memux/memux.tar.xz"):
                print("\nCould not download files. Check internet connection.")
                print("\033[1;31;40m FAILED :(")
                quit()
            sub.call("xz -dv /tmp/memux/memux.tar.xz;tar -xvf /tmp/memux/memux.tar -C /tmp/memux", shell=True)
            sub.call("mkdir -p " + installdir, shell=True)
            sub.call("mv /tmp/memux/r/* " + installdir, shell=True)

clone()

if installdir[len(installdir)-1] == '/':
    installdir = installdir[0:len(installdir)-1]
if distro != 'solus':
    sub.call("pip3 install --user psutil", shell=True)

sub.call("gsettings set com.gexperts.Tilix.Profile:/com/gexperts/Tilix/profiles/2b7c4080-0ddd-46c5-8f23-563fd3ba789d/ use-custom-command true", shell=True)
sub.call("gsettings set com.gexperts.Tilix.Profile:/com/gexperts/Tilix/profiles/2b7c4080-0ddd-46c5-8f23-563fd3ba789d/ custom-command 'python3 " + installdir + "/main.py'", shell=True)


##########################
### Check installation ###
##########################


 ### Check dconf keys
good = True
sub.call('gsettings get com.gexperts.Tilix.Profile:/com/gexperts/Tilix/profiles/2b7c4080-0ddd-46c5-8f23-563fd3ba789d/ use-custom-command > /tmp/memux/use-custom-command', shell=True)
sub.call('gsettings get com.gexperts.Tilix.Profile:/com/gexperts/Tilix/profiles/2b7c4080-0ddd-46c5-8f23-563fd3ba789d/ custom-command > /tmp/memux/custom-command', shell=True)
ucc = open("/tmp/memux/use-custom-command", 'r')
cc = open("/tmp/memux/custom-command", 'r')
cc = cc.readline(len(installdir)+40)
ucc = ucc.readline(4)
#if str(cc) != "'python3 " + installdir + "/main.py'":
#    print("\nCould not set dconf database keys. (variable cc)")
#    print(str(cc))
#    print("'python3 " + installdir + "/main.py'")
#    print("\033[1;31;40m FAILED :(")
#    quit()
#    good = False
if str(ucc) != "true":                              
    print("\nCould not set dconf database keys.")
    print(str(ucc))
    print("\033[1;31;40m FAILED :(")
    quit()
    good = False
shutil.rmtree("/tmp/memux")
if good:
    if not os.path.exists("/home/"+usr+"/.config/memux/installed"):
        print("Run this script again to upate memux.")
    else:
        print("Memux has been updated.")
    print("\033[1;32;40m DONE :)")
    sub.call("mkdir -p /home/"+usr+"/.config/memux;touch /home/"+usr+"/.config/memux/installed", shell=True)
